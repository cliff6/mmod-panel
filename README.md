# MMOD-Panel [![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

MMOD Panel is an upgrade to the Topbar in Gnome3, creating a customizable panel and providing options for fine-tuning your Desktop Experience.

Supported Gnome-shell versions:
* 3.38
* 3.36.6
* 3.36.4
* 3.36.3
* 3.36
* 3.35.91
* 3.35
* 3.34
* 3.32
* 3.30
* 3.28
* 3.26.2
* 3.26
* 3.24
* 3.22
* 3.20
* 3.18
* 3.16.2
* 3.16
* 3.14.4
* 3.14
* 3.12.2
* 3.12
* 3.10

Check out MMOD-Panel at [extensions.gnome.org](https://extensions.gnome.org/extension/898/mmod-panel).

## Features

* Set comfort levels to provide theme support and fine-tune the overall look and feel of the panel.
* Set the location/position of the panel (bottom by default).
* Add a button to the panel in place of the activities link, using an icon of your preference.
* Auto-hide the panel when not active/in-focus (makes use of pressure/gesture for showing the panel).
* Display and manage your favorites/running apps directly on the panel.
* Move the date menu to the aggregate/tray area.
* Access and manage your extension preferences directly from the aggregate menu.
* Customize behavior of the overview and panel(hot-corners/animations/effects) to suit your preferences.
* More to come soon!

## Getting Started

Upon installation you will find yourself greeted by MMOD-Panel's Gnome Extension Preferences Window.  You may browse through the settings and make changes as you like.

### Install

Please see the COPYING file for copyright information. You may also view the AUTHORS file for a list of contributors to this version of the software.

#### Option A. Direct Installation

* Using IceWeasle or FireFox, visit extensions.gnome.org in order to install this extension.

#### Option B. Download the distribution

* Use Gnome Tweak Tool to install the extension.

#### Alternate Manual Install

Manual installation can be performed. The following guide will result in two different ways to go about it, depending on your comfort level with terminal

##### Preparation

Ensure you have the following dependencies installed:

* NodeJS

##### Getting the source

Open a terminal *change instructions appropriately for your distribution* and change to your home directory:

```bash
cd ~/
```

Create a temporary directory and change to it:

```bash
mkdir tmp
cd tmp
```

Download the extension and change to the extension directory:

```bash
git clone https://gitlab.com/mmod/mmod-panel && cd mmod-panel
```

##### Building the Source

Install build dependencies for the extension:

```bash
npm install .
```

Build the extension

```bash
npm run build
```

**Or** *depending on which tag you're building (i.e. npm scripts didn't exist)*:

```bash
gulp
```

Finally, copy the extension files:

```bash
mkdir -p ~/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com
cp -R build/prep/* ~/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com
```

##### Building the Archive

Perhaps you want to build the archive and extract it manually into the appropriate directory (?? You never know, lol.):

```bash
npm run build:release
```

**Or** *again, depending on which tag you're building (i.e. npm scripts doesn't exist)*:

```bash
gulp build-release
```

Finally, extract the contents of `./build/release/mmod-panel.zip` to `~/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com`.

* Be sure it's the ***contents*** of your mmod-panel.zip, and not the resulting top-level directory that goes into `mmod-panel@mmogp.com`.

##### Alternate Manual Install Conclusion

That's how you'd install the extension manually. You can track changes to the repository and update and new releases are made available as well.

###### Downloading Updates

Simply pull changes to the repository when a new tag (version) is released:

```bash
git pull origin
```

Then go ahead and follow the instructions from [Building the Source](#building-the-source).

### Uninstall

#### Option A. Gnome Tweak Tool

* Use Gnome Tweak Tool to uninstall the extension.

#### Option B. Use IceWeasle or FireFox

* Visit extensions.gnome.org in order to uninstall this extension.

### Option C. Manual Uninstall

* Delete the `mmod-panel@mmogp.com` directory from `~/.local/share/gnome-shell/extensions/`
  `rm -rf !/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com`

## Inspiration
This project is loosely based on the Panel Settings extension:

https://github.com/eddiefullmetal/gnome-shell-extensions/tree/master/panelSettings%40eddiefullmetal.gr

Sadly, Panel Settings has not seen any maintenance in over 2 years, though this is why I decided to create MMOD Panel.

I also took inspiration from the following Gnome extensions:

* System-Monitor
* Taskbar
* DashToDock.

For those of you who are wondering, the theme used in the screen shot (found at [extensions.gnome.org](https://extensions.gnome.org/extension/898/mmod-panel)) is the Zukitwo-Dark-Shell Shell Theme; everything else is default Gnome on Debian Jessie. However, the author of the aforementioned shell theme has changed the name for various reasons to Ciliora-Prima-Shell - which can be found here:  http://gnome-look.org/content/show.php?content=165096

## Contributors/Translators

German Translation(s) for MMOD-Panel courtesy of:

* Jonius Zeidler <jonatan_zeidler@gmx.de>"

## Notes

* I personally use the Zukitwo-Dark-Shell (now termed Ciliora-Prima-Shell) Gnome Shell theme, which you can find over at gnome-look.org, with this extension (and user themes obviously) only, on Debian Jessy.
* Auto-hide works well, hovering your mouse over an empty portion of the panel will not keep it open; it only stays open when activities, app preferences, favorites, aggregate, and date buttons/icons are hovered, and when the overview, date menu, or aggregate menu are open (note that the app preferences menu does not keep the panel shown either).
* The link to the website for feedback is currently to the homepage of our web site and there is no respective form or area to leave feedback; it will be up in the very near future.
* Position modifications were changed to wait for realization (excludes style modification, only position).
* The shell support for MMOD-Panel has been changed to 3.10+ rather than 3.8.
* The main VCS in use is now our Gitlab instance over at ~~code.mmogp.com~~ [GitLab](https://gitlab.com/mmod/mmod-panel).  ~~However, we're still mirroring to Github.com, due to popular demand.~~

## Development

Feel free to fork the repository and submit pull requests.

## Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified Online Gathering Place network, and all of its Free & Open Source Software.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

### Created with:

[Visual Studio Code](https://code.visualstudio.com/)
