# Installation Instructions

Please see the COPYING file for copyright information. You may also view the AUTHORS file for a list of contributors to this version of the software.

## Install Options

### Option A. Direct Installation

* Using IceWeasle or FireFox, visit extensions.gnome.org in order to install this extension.
* Using Google Chrome with the Gnome Extensions extension, visit extensions.gnome.org in order to install this extension.

### Option B. Download the distribution

* Use Gnome Tweak Tool to install the extension.

#### Alternate Manual Install

Manual installation can be performed. The following guide will result in two different ways to go about it, depending on your comfort level with terminal

##### Preparation

Ensure you have the following dependencies installed:

* NodeJS

##### Getting the source

Open a terminal *change instructions appropriately for your distribution* and change to your home directory:

```bash
cd ~/
```

Create a temporary directory and change to it:

```bash
mkdir tmp
cd tmp
```

Download the extension and change to the extension directory:

```bash
git clone https://gitlab.com/mmod/mmod-panel && cd mmod-panel
```

##### Building the Source

Install build dependencies for the extension:

```bash
npm install .
```

Build the extension

```bash
npm run build
```

**Or** *depending on which tag you're building (i.e. npm scripts didn't exist)*:

```bash
gulp
```

Finally, copy the extension files:

```bash
mkdir -p ~/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com
cp -R build/prep/* ~/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com
```

##### Building the Archive

Perhaps you want to build the archive and extract it manually into the appropriate directory (?? You never know, lol.):

```bash
npm run build:release
```

**Or** *again, depending on which tag you're building (i.e. npm scripts doesn't exist)*:

```bash
gulp build-release
```

Finally, extract the contents of `./build/release/mmod-panel.zip` to `~/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com`.

* Be sure it's the ***contents*** of your mmod-panel.zip, and not the resulting top-level directory that goes into `mmod-panel@mmogp.com`.

##### Alternate Manual Install Conclusion

That's how you'd install the extension manually. You can track changes to the repository and update and new releases are made available as well.

###### Downloading Updates

Simply pull changes to the repository when a new tag (version) is released:

```bash
git pull origin
```

Then go ahead and follow the instructions from [Building the Source](#building-the-source).

Enjoy the extension!

## Uninstall

### Option A. Gnome Tweak Tool

* Use Gnome Tweak Tool to uninstall the extension.

### Option B. Use IceWeasle or FireFox

* Visit extensions.gnome.org in order to uninstall this extension.

### Option C. Manual Uninstall

* Delete the `mmod-panel@mmogp.com` directory from `~/.local/share/gnome-shell/extensions/`
  `rm -rf !/.local/share/gnome-shell/extensions/mmod-panel@mmogp.com`